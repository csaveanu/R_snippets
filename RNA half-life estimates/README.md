A simple script to obtain estimates of the half-life for an RNA based on a "no-lag" exponential decay curve. The script contains a function that tests for errors, as it was used to compute half-life values for a few thousand transcripts in a SLAM-Seq experiment. It can also be used with other results, such as those coming from transcription shut-off experiments combined with RT-qPCR.

A typical result that can be obtained with this script (invented data):

![alt output from ggplot2][output]

[output]: ./degradationplotRNA.png
