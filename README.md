# A repository of R and Python snippets, mostly for creating graphical visualization of data.

The small script fragments with their explanations can be useful for:

- representing results as heatmaps (Categorical heatmaps, R)
- combine tables with a kind of very crude left join written in Python (Get rows py3)
- represent results as barplots (Tridimensional barplots in R)
- density coloured scatter plots that avoid overplotting (with ggplot2, R)
- estimating half-life for RNA with a simple exponential decay (RNA half-life estimate, R)

Museum-level scripts are also present in this repository, to keep a trace of their existence (Polysome Rider)
