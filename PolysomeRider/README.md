`Polysome Rider` is a small program that recovers digitally converted analog data, displays it and allows it to be saved. The program has been written in 2004, using [Python](https://www.python.org/) 2.3 or 2.4 and still runs on the original machine as of March 2022. It is, in a way, the successor of a similar program that I wrote in 1997-1998, to be able to measure enzyme activity by following the increase in the absorbance of one of the products. The original program was written using [Borland Delphi](https://en.wikipedia.org/wiki/History_of_Delphi_(software)#Early_Borland_years_(1995%E2%80%932003)), one of the most user-friendly rapid application development tools that I had the pleasure to use. The language used by Borland Delphi was a version of Pascal.

There are modern alternatives to this program, and we will be forced to switch to one, the day the old computer running it no longer boots. Both hardware and software are now quite different for converting an analog signal to a digital one, and an [Arduino](https://www.arduino.cc/) project is waiting for that in a drawer...

A screenshot (from 2014), showing some of the available options for "real-time" display of the collected data:

![alt text](polysomeridershot.png "Polysome Rider screenshot")
