**UPDATE**: a more complex and versatile online alternative of this tool, for beautiful and configurable heatmaps is [Morpheus](https://software.broadinstitute.org/morpheus/).

The script `generate_heatmap.R` can be used to represent a table of data in the form of an adjusted heat map. It uses two packages that need to be installed, `ggplot2` and `reshape2`. The input of the script is a tab delimited file text that looks like this:

![alt input table](./inputtable.png)

What the script does:

1. Takes the maximum of each line (thus maximum value for each gene, in our example) and divides the values with that maximum. Thus, for each gene, we get values between 0 and 1 (per row).

2. Transforms the obtained values to categories: for example, if a value is between 0.25 and 0.5, that value will belong to the "25%-50%" category

3. Use `ggplot2` to generate a graphical representation of the categories in a table. If the plot is satisfactory, it can be saved, with the `ggsave` function.

![alt output from ggplot2][output]

[output]: ./heatplotexampleoutput.png


The final results can be further enhanced and modified in Inkscape or Adobe illustrator.
