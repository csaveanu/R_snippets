"""
Pydaq - a class that allows easy reading of data
from a data acquisition card making use of the
nidaq32.dll. Might be adaptable to the new mx nidaq.

The source for this program
was a message on www.mail-archive.com/pygtk@daa.com.au/
msg07496.html, signed Todd G. Gardner - pydaq Jan 2004
The different comments are mine (Cosmin, august 2004)
based on the NI-DAQ API function reference

This script makes heavy use of the ctypes module
that allows direct access to C library functions
"""

from ctypes import *

# Create instance of traditional nidaq dll
ni=windll.nidaq32

class pydaq:
  def __init__(self):
     """variable declaration"""
     self.deviceNumber=c_int(1)
     #the device number is generally 1
     self.chan=c_int(0)
     #the channel you're going to listen to
     self.voltage=c_double(1.0)
     #the read voltage - a float double value
     self.pvoltage=pointer(self.voltage)
     #a pointer to the value returned
     #this is the output of the AI_VRead function
     self.gain=c_int(-1)
     #amplification, -1 means multiplying the signal by 0.5
     self.num=100 #Number of data points
     self.sampleRate=c_double(1000)
     self.max=10.0
     self.min=-10.0
     self.bits=16

  def AI_Configure (self, deviceNumber=c_int(1), chan=c_int(0),
                              inputMode=c_int(0), inputRange=c_int(10), 
                              polarity=c_int(0), driveAIS=c_int(0)):
    """
    configure analog input task
    by providing values to a card and channel
    """
    self.AI_ConfigureStatus = ni.AI_Configure (deviceNumber, chan,
                                      inputMode, inputRange, polarity, driveAIS)
    #print "AI_Configure status =", self.AI_ConfigureStatus
    return self.AI_ConfigureStatus
    
  def AI_VRead (self, deviceNumber=c_int(1), chan=c_int(0), 
                    gain=c_int(-1), pvoltage=pointer(c_double(1.0))):
    """
    voltage reading from a device and channel with
    a certain gain. The results go to an adress, a
    pointer to that adress is in pvoltage
    Returns a tuple - status, voltage
    """
    self.AO_VReadStatus = ni.AI_VRead (deviceNumber, chan, gain, pvoltage)
    #print "AI_VRead status =", self.AO_VWriteStatus, "voltage =",pvoltage.contents
    return self.AO_VReadStatus, pvoltage[0]

  def DAQ_Clear (self, deviceNumber):
    """
    Cancels the current DAQ operation and
    reinitializes the DAQ circuitry.
    """
    self.DAQ_Clear_status = ni.DAQ_Clear (self.deviceNumber)
    return self.DAQ_Clear_status

  def AI_Clear (self, deviceNumber=c_int(1)):
    """
    clear the data acquisition task
    AI_Clear clears the analog input circuitry and
    empties the analog input FIFO memory.
    """
    self.AI_ClearStatus = ni.AI_Clear (self.deviceNumber)
    #print "AI_Clear status =", self.AI_ClearStatus
    return self.AI_ClearStatus

if __name__ == '__main__':
  pd=pydaq()
  chan = c_int(1)
  volt = pointer(c_double(0.1))
  deviceNo=c_int(1)

  print pd.DAQ_Clear(pd.deviceNumber)
  readtuple = pd.AI_VRead(chan = chan, gain=c_int(10), pvoltage = volt)
  print readtuple[1]/0.1*2.56 #2.56 is the scale on the spectro
  print readtuple[1], "volts"
  print pd.DAQ_Clear(pd.deviceNumber)