"""
filter data by averaging on a number of points
input - text file with two tab delimited columns
output - filtered text file
parameter - size of the window
"""
import sys

INTTRANSFORM = 1000
if len(sys.argv) < 4:
	print "Three arguments needed: input file name, output file name, window size"
	sys.exit(0)
else:
	file_in_name = sys.argv[1]
	file_out_name = sys.argv[2]
	wsize = int(sys.argv[3])

fin = open(file_in_name, "r")
data = []
for line in fin:
	lst = line.strip('\r\n ').split('\t')
	data.append([int(float(lst[0])), int(INTTRANSFORM*float(lst[1]))])
fin.close()

filtdata = []
sum = 0
prevsum = 0
for i in range(int(wsize/2)):
	filtdata.append([data[i][0], data[i][1]/INTTRANSFORM])
	prevsum=prevsum+data[i][1]

for i in range(int(wsize/2),len(data)-wsize/2-1):
	sum = prevsum - data[i-wsize/2 -1][1] + data[i + wsize/2][1]
	prevsum = sum
	#print sum
	filtdata.append([data[i][0], sum/float(wsize)/float(INTTRANSFORM)])
	
fout = open(file_out_name, "w")
for line in filtdata:
	fout.write('%d\t%.3f\n'%(line[0], line[1]))